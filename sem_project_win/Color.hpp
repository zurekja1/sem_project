#include <string>
#include <iostream>
#include "SDL2/SDL_pixels.h"

// exception of wrong color format
struct ColorConvertException : std::exception {
    virtual const char* what() const throw();
};

// color class for holding RGBA values
class Color {
    private:
        // hex code of color convert to decimal RGBA values
        uint8_t htd(char hex) const;

    public:
        int R, G, B, A;

        //constructor
        Color();

        //constructor converting str hex to int values
        Color(const std::string& hex);

        //copy constructor
        Color(const Color& c);

        // overiden operator =
        Color& operator=(const Color& c);

        // convert inner values RGBA to SDL_Color
        SDL_Color toSDL_Color() const;

        #if TEST
            // output of object
            friend std::ostream& operator<< (std::ostream& out, const Color& c);
        #endif
};
