#include "Element.hpp"

Element::Element() 
    : atomic_number(0), symbol(""), name(""), color(Color()) {}

Element::Element(const unsigned int atomic_number,
                 const std::string& symbol,
                 const std::string& name,
                 const std::string& color_s) 
    : atomic_number(atomic_number), symbol(symbol), name(name), color(Color(color_s)) {}

//destructor
Element::~Element() {
    symbol.clear();
    name.clear();
}

// copy constructor
Element::Element(const Element& e) {
    this->atomic_number = e.atomic_number;
    this->symbol = e.symbol;
    this->name   = e.name;
    this->color  = e.color;
}

// overoaded operator =
Element& Element::operator=(const Element& e) {
    if (this == &e)
        return *this;
    this->atomic_number = e.atomic_number;
    this->symbol = e.symbol;
    this->name   = e.name;
    this->color  = e.color;
    return *this;
}

// gets elements atomic number
unsigned int Element::getAN() const {
    return atomic_number;
}

// sets elements atomic number
void Element::setAN(unsigned int n_AN) {
    atomic_number = n_AN;
}

// gets elements color
Color Element::getColor() const {
    return color;
}

// gets elements symbol
std::string Element::getSymbol() const {
    return symbol;
}

// overloaded operator ==
bool operator==(const Element& lhs, const Element& rhs) {
    return lhs.name == rhs.name;
}
// overloaded operator !=
bool operator!=(const Element& lhs, const Element& rhs) {
    return lhs.name != rhs.name;
}

#if TEST
    // output of object
    std::ostream& operator<<(std::ostream& os, const Element& e) {
        return os << e.atomic_number << " " << e.symbol << " " << e.name << " " << e.color;
    }
#endif