#include "SDL2/SDL_render.h"
#include "SDL2/SDL_pixels.h"

#include "Window.hpp"
#include "Layout.hpp"

#define RENDER_FLAGS (SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)

// exception, renderer is not created
struct RenderException : std::exception {
    virtual const char* what() const throw();
};

// class representingc sdl_window
class Render {
    private:
        SDL_Renderer* render;
        SDL_Color bg_c;
        Layout w_layout;

    public:
        // constructor
        Render(const Window& window);

        //destructor
        ~Render();

        // return pointer of sdl_rendere
        SDL_Renderer* getSDL_Render() const;

        // set color to draw for renderer
        void setColor(const SDL_Color& c);

        // present actual renderer
        void show();

        // clear actual renderer
        void clear();

        // resize game panel layout
        void resize(SDL_Window* win);

        // return actual layout of renderer
        Layout getW_Layout() const;
};
