#include "Render.hpp"

// exception, renderer is not created
const char* RenderException::what() const throw() {
    return "Renderer creation failed";
};

// constructor
Render::Render(const Window& window)
    : render(SDL_CreateRenderer(window.getSDL_Window(), -1, RENDER_FLAGS)),
      bg_c(SDL_Color{14, 4, 41, 255}),
      w_layout(Layout(WIN_WIDTH, WIN_HEIGHT)) {
        if (render == nullptr) throw RenderException();
    }

//destructor
Render::~Render() {
    SDL_DestroyRenderer(render);
}

// return pointer of sdl_rendere
SDL_Renderer* Render::getSDL_Render() const {
    return render;
}

// set color to draw for renderer
void Render::setColor(const SDL_Color& c) {
    SDL_SetRenderDrawColor(render, c.r, c.g, c.b, c.a);
}

// present actual renderer
void Render::show() {
    SDL_RenderPresent(render);
}

// clear actual renderer
void Render::clear() {
    SDL_Color c;
    SDL_GetRenderDrawColor(render, &c.r, &c.g, &c.b, &c.a);
    SDL_SetRenderDrawColor(render, bg_c.r, bg_c.g, bg_c.b, bg_c.a);
    SDL_RenderClear(render);
    // #if TEST
    //     SDL_SetRenderDrawColor(render, 128, 128, 128, 255);
    //     SDL_RenderDrawRect(render, w_layout.getBorder());
    // #endif
    SDL_SetRenderDrawColor(render, c.r, c.g, c.b, c.a);
}

// resize game panel layout
void Render::resize(SDL_Window* win) {
    int width, height;
    SDL_GetWindowSize(win, &width, &height);
    w_layout.resize(width, height);
}

// return actual layout of renderer
Layout Render::getW_Layout() const {
    return w_layout;
}
