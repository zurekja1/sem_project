#include "GameState.hpp"
#include "Render.hpp"
#include "SDL2/SDL_timer.h"

#define ELEMENT_SIZE 100

#define DELAY 10
#define MERGE_DELAY 50
#define FPS 45
#define ANIM_DELAY 1

#define ATOM_TEMPLATE "textures/atom.bmp"

#define SIGN_TEMPLATE "textures/signs.bmp"
#define SIGN_WIDTH    150
#define SIGN_HEIGHT   30
#define SIGN_LEN      5

#define NUMBER_TEMPLATE "textures/numbers.bmp"
#define NUMBER_WIDTH    300
#define NUMBER_HEIGHT   31
#define NUMBER_LEN      10

#define UPPERS_TEMPLATE "textures/uppers.bmp"
#define LOWERS_TEMPLATE "textures/lowers.bmp"
#define CASE_WIDTH    782
#define CASE_HEIGHT   40
#define CASE_LEN      26

#define BG_TEMPLATE "textures/background.bmp"
#define BG_WIDTH    500
#define BG_HEIGHT   500

// exception of wrong bmp file loading
struct LoadBMPException : std::exception {
    virtual const char* what() const throw();
};

class Drawer {
    private:
        Render* render;
        SDL_Surface* atom_image;
        SDL_Texture* atom_texture;

        SDL_Surface* signs_image;
        SDL_Texture* signs_texture;

        SDL_Surface* numbers_image;
        SDL_Texture* numbers_texture;

        SDL_Surface* uppers_image  , * lowers_image;
        SDL_Texture* uppers_texture, * lowers_texture;

        SDL_Surface* background_image;
        SDL_Texture* background_texture;

        // get centerer position of window for element
        SDL_Rect getCenterPosition(const SDL_Rect* border);

        // draw text on pos area by elements atomic number and symbol
        void drawText(unsigned int an, const std::string symbol, const SDL_Rect* pos);

        // draw element to inserted positon on window
        void drawElement(const Element* e, SDL_Rect* pos);

    public:

        //constructor, ren subs of SDL_Renderer
        //creats needed textures of atom, signs, numbers, uppers, lowers and background
        Drawer(Render* ren);

        //destructor
        //frees textures of atom, signs, numbers, uppers, lowers and background
        ~Drawer();

        //draw image of bacground into window
        void drawBackground();

        //draw held Element into center of window
        void drawHeldElement(const Element& h_e);

        //draw Element form field around of center of window
        void drawElementField(const std::vector<Element*>& field);

        // draw effect of showing held element with field around
        void drawElementShowEffect(const std::vector<Element*>& field, const Element& h_elem);

        // draw effect of insrting elemnt form center to position of field
        void drawInsertEffect(const std::vector<Element*>& field, const Element& h_elem,
                              const std::size_t& pos);

        // draw effect of removing element from position to center of window
        void drawRemoveEffect(const std::vector<Element*>& field, const std::size_t& pos);

        // drawing elements merging into proton element
        void drawMergeEffect(const std::vector<Element*>& field);

        // drawing effect before first insert
        void drawStartEffect(const std::vector<Element*>& field, const Element& h_elem);

        // drawing effect after reach last element
        void drawEndingEffect(const std::vector<Element*>& field, const Element& h_elem);

        // drawing effect of reseting elements
        void drawResetEffect(const std::vector<Element*>& field, const Element& h_elem);

        // drawing signs for last 3 elements
        void drawCountdownEffect(const std::vector<Element*>& field);
};