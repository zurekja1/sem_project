#include "Color.hpp"

// exception of wrong color format
const char* ColorConvertException::what() const throw() {
    return "Color converting failed";
}

//constructor
Color::Color()
    : R(0), G(0), B(0), A(0) {}

//constructor converting str hex to int values
Color::Color(const std::string& hex)
    : R((htd(hex[0]) << 4) + htd(hex[1])),
      G((htd(hex[2]) << 4) + htd(hex[3])),
      B((htd(hex[4]) << 4) + htd(hex[5])),
      A((htd(hex[6]) << 4) + htd(hex[7])) {
        if ((R < 0 || R > 256) || (G < 0 || G > 256) ||
            (B < 0 || B > 256) || (A < 0 || A > 256) ||
            (hex.size() != 8)) throw ColorConvertException();
      }

//copy constructor
Color::Color(const Color& c) {
    this->R = c.R;
    this->G = c.G;
    this->B = c.B;
    this->A = c.A;
}

// overiden operator =
Color& Color::operator=(const Color& c) {
    if (this == &c)
        return *this;
    this->R = c.R;
    this->G = c.G;
    this->B = c.B;
    this->A = c.A;
    return *this;
}

// hex code of color convert to decimal RGBA values
uint8_t Color::htd(char hex) const {
    return '0' <= hex && hex <= '9' ?
        (uint8_t) (hex - '0') : 10 + (uint8_t) (hex - 'a');
}

// convert inner values RGBA to SDL_Color
SDL_Color Color::toSDL_Color() const {
    return SDL_Color{(uint8_t) R, (uint8_t) G, (uint8_t) B, (uint8_t) A};
}

#if TEST
    // output of object
    std::ostream& operator<< (std::ostream& out, const Color& c) {
        return out << "{" << c.R << " " << c.G << " " << c.B << " " << c.A << "}";
    }
#endif