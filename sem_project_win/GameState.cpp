#include "GameState.hpp"

// construcotr
GameState::GameState(std::string element_list_file)
      : element_list(readElementFile(element_list_file)),
        e_range(START_ELEMENT_RANGE),
        field(EField())
        {
            std::srand(std::time(nullptr)); // use current time as seed for random generator
            held_element = element_list[randIER()];
            std::vector<Element> first_elements;
            for (size_t i = 0; i < ELEMENT_START_COUNT; i++)
                first_elements.push_back(element_list[randIER()]);
            field.insert(first_elements);
        }

// destructor
GameState::~GameState() {}

// random int in element range
inline int GameState::randIER() const {
    return e_range.first + (std::rand() % (e_range.second - e_range.first));
}

// create new held element form element range
void GameState::createElement() {
    std::vector<int> prob = {(int)(field.getSize() * 2 / 3), (int)(field.getSize() / 3)};
    for (size_t i = 0; i < field.getSize(); i++)
        if (*field[i] == proton) prob[0]++;
        else                     prob[1]++;
    int r = (prob[0] + prob[1]);
    if (((rand() % (r * r)) < (prob[1] * prob[1])) &&
        ((prob[0] - (int)(field.getSize() * 2 / 3)) < 5) &&
        field.getSize() > 3) {
        if ((rand() % 100) > 10) {
            int r = rand() % 100;
            if      (r > 75) held_element = Element(proton);
            else if (r > 50) held_element = Element(electron);
            else if (r > 25) held_element = Element(duplicator);
            else             held_element = Element(mover);
        } else held_element = Element(black_hole);
    } else {
        held_element = element_list[randIER()];
    }
}

// get actual held element
Element GameState::getHeldElement() const {
    return held_element;
}

// insert held element to selected position
void GameState::insertElement(std::size_t pos) {
    field.insert(held_element, pos);
}
// removes element from field at selected position and set held element to electron
void GameState::changeElement(std::size_t pos) {
    delete field.remove(pos);
    held_element = Element(proton);
}

// set held element as same element from field at selected position
void GameState::duplicateElement(std::size_t pos) {
    held_element = Element(*field[pos]);
}

// erase and delete element from field at selected position
void GameState::destroyElement(std::size_t pos) {
    delete field.remove(pos);
}

// erase element from field from selected position and set held element
void GameState::moveElement(std::size_t pos) {
    Element* m = field.remove(pos);
    held_element = Element(*m);
    delete m;
}

// return true, if field is mergeable
bool GameState::isMergeable() const {
    if (field.getSize() < 3) return false;
    for (size_t i = 0; i < field.getSize(); i++)
        if (*field[i] == proton && field.isBridge(i, 1)) return true;
    return false;
}

// merge fields elements
void GameState::merge() {
    size_t proton_pos; // position of mergeable proton
    for (size_t i = 0; i < field.getSize(); i++) { // finding mergeable proton
        if (*field[i] == proton && field.isBridge(i, 1)) {
            proton_pos = i;
            break;
        }
    }
    Element *l, *r, *p = field[proton_pos];
    if (proton_pos != field.getSize() - 1) { // removing neighboring elements
        r = field.remove((field.getSize() + proton_pos + 1) % field.getSize());
        l = field.remove((field.getSize() + proton_pos - 1) % field.getSize());
    } else {
        l = field.remove((field.getSize() + proton_pos - 1) % field.getSize());
        r = field.remove((field.getSize() + proton_pos    ) % field.getSize());
    }
    p->setAN((p->getAN() < l->getAN() ? l->getAN() : p->getAN()) + 1); // setting new element atomic number
    delete l; delete r; // deleting neighboring elements
    for (size_t i = 0; i < field.getSize(); i++) { // replacing proton elements for new element
        if ((*field[i] == proton) && (field[i]->getAN() != 0) && (!field.isBridge(i, 1))) {
            Element* r = field.remove(i);
            field.insert(element_list[r->getAN() - 1], i);
            delete r;
        }
    }
}

// set new range for element generating
void GameState::setRange() {
    unsigned int min = 666, max = 0;
    for (size_t i = 0; i < field.getSize(); i++) {
        if (min > field[i]->getAN()) min = field[i]->getAN();
        if (max < field[i]->getAN()) max = field[i]->getAN();
    }
    #if TEST
        std::cout << "new range: " << min << " " << max << std::endl;
    #endif
    if (min != max) {
        if (e_range.first  < min - 1 && min != 0) e_range.first = min - 1;
        if (e_range.second < max - 1 && max != 0) e_range.second = max - 1;
    }
    #if TEST
        std::cout << "final range: " << min << " " << max << std::endl;
    #endif
}

// return field of elements copy
std::vector<Element*> GameState::getField() {
    std::vector<Element*> tmp_field;
    for (size_t i = 0; i < field.getSize(); i++)
        tmp_field.push_back(field[i]);
    return tmp_field;
}

// get amid position of element from angle
std::size_t GameState::getAmidPosition(float angle) {
    float phi = M_PI * 2 / field.getSize();
    for (size_t i = 0; i < field.getSize(); i++)
        if (angle <= phi * (i + 1))
            return i + 1;
    return 0;
}

// get accurate position of element form angel
std::size_t GameState::getAccurPosition(float angle) {
    float phi = M_PI * 2 / field.getSize();
    for (size_t i = 0; i < field.getSize(); i++)
        if (angle <= phi * (i + 1) - phi / 2)
            return i;
    return 0;
}

// clear field, set elemnt range and inser few elements for new game
void GameState::reset() {
    e_range = START_ELEMENT_RANGE;
    field = EField();
    std::srand(std::time(nullptr));
    held_element = element_list[randIER()];
    std::vector<Element> first_elements;
    for (size_t i = 0; i < ELEMENT_START_COUNT; i++)
        first_elements.push_back(element_list[randIER()]);
    field.insert(first_elements);
}

// return true for state of fullfilled filed of elements
bool GameState::inProgress() const {
    return field.getSize() != MAX_ELEMENT_COUNT;
}

//return true if field includes last element form element list
bool GameState::hasLastElement() {
    if (e_range.second < 100) return false;
    std::size_t last_ep = element_list.size() - 1;
    for (size_t i = 0; i < field.getSize(); i++)
        if (field[i]->getAN() == element_list[last_ep].getAN()) {
            return true;
        }
    return false;
}

// return last element from element list and set held element on last element from element list
Element GameState::getsetLastElement() {
    held_element = Element(element_list[element_list.size() - 1]);
    return held_element;
}

#if TEST
    // output of object
    std::ostream& operator<<(std::ostream& out, const GameState& gs) {
        out << "element range: {" << gs.e_range.first << ", " << gs.e_range.second << "}" << std::endl;
        out << "held element:  " << gs.held_element << std::endl;
        out << gs.field;
        return out;
    }
#endif