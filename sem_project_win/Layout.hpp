#include "SDL2/SDL_rect.h"
#include <utility>
#include <math.h>

#define HOR_BORDER 25
#define VER_BORDER 25

// class for centering gaming panel
class Layout {
    private:
        SDL_Rect border;

    public:
        // constructor
        Layout();

        // constructor, make border of game panel
        Layout(int width, int height);

        // destructor
        ~Layout();

        // reshape border of game panel
        void resize(int width, int height);

        // return poiter of game panel
        SDL_Rect* getBorder();

        // return true, if inserted coordinates are inside of game panel
        bool isClicked(int x, int y);

        // return angel in radians from center to inserted coordinates
        float getAngleFromCenter(int x, int y);
};