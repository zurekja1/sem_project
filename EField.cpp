#include "EField.hpp"

// exception of wrong field position
const char* EFieldArrayException::what() const throw() {
    return "Out Of Array Exception";
}

// exception of wrong file of element to read
const char* FileOfElementsClosed::what() const throw() {
    return "File was not found Exception";
}

// exception of wrong elemet to read
const char* FileOfElementsReadException::what() const throw() {
    return "File was not readable Exception";
}

// constructor
EField::EField() 
    : field(std::vector<Element*>{}) {}

// destructor, removes and delete all inserted elements
EField::~EField() {
    for (Element* e : field)
        delete e;
    field.clear();
}

// insert element to selected position
void EField::insert(const Element& nElem, std::size_t pos) {
    if (field.size() < pos) // worng position
        throw EFieldArrayException();
    field.insert(field.begin() + pos, new Element(nElem));
}

// insert vector of elements to selected position
void EField::insert(std::vector<Element> nElems, std::size_t pos) {
    if (field.size() < pos) // worng position
        throw EFieldArrayException();
    for (size_t i = 0; i < nElems.size(); i++)
        field.insert(field.begin() + pos + i, new Element(nElems[i]));
}

// remove element from selected position
Element* EField::remove(std::size_t pos) {
    if (field.size() <= pos) // worng position
        throw EFieldArrayException();
    Element* e = field[pos];
    field.erase(field.begin() + pos);
    return e;
}

// remove elements from selected position from -> to
std::vector<Element*> EField::remove(std::size_t from, std::size_t to) {
    if (field.size() <= from || field.size() <= to)
        throw EFieldArrayException(); // worng position
    std::vector<Element*> removed = {};
    if (from <= to) {
        for (size_t i = from; i <= to; i++) {
            removed.push_back(field[from]);
            field.erase(field.begin() + from);
        }
    } else {
        size_t f_size = field.size();
        for (size_t i = 0; i < f_size - from; i++) {
            removed.push_back(*(field.end() - 1));
            field.erase(field.end() - 1);
        }
        for (size_t i = 0; i <= to; i++) {
            removed.insert(removed.begin(), field[0]);
            field.erase(field.begin());
        }
    }
    return removed;
}

// overloaded operator []
Element* EField::operator[](std::size_t idx) {
    if (field.size() <= idx) // worng position
        throw EFieldArrayException();
    return field[idx];
}

// overloaded operator []
const Element* EField::operator[](std::size_t idx) const {
    if (field.size() <= idx) // worng position
        throw EFieldArrayException();
    return field[idx];
}

// get actual size of element field
std::size_t EField::getSize() const {
    return field.size();
}

// return true if at pos is proton and around same elements
bool EField::isBridge(std::size_t pos, size_t width) const {
    std::size_t r_pos = (field.size() + pos + width) % field.size();
    std::size_t l_pos = (field.size() + pos - width) % field.size();
    if (l_pos == r_pos) return false; // musnt be the same element from from field
    if (*field[l_pos] == proton || *field[l_pos] == proton) return false; //mustnt be all protons
    return *field[l_pos] == *field[r_pos];
}

#if TEST
    // output of object
    std::ostream& operator<<(std::ostream& out, const EField& ef) {
        out << "field of elements\n";
        for (const Element* e : ef.field)
            out << " - " << e << ' ' << *e << '\n';
        return out;
    }
#endif

std::vector<Element> readElementFile(const std::string& f_name) {
    std::vector<Element> r_elements;
    std::ifstream file(f_name);
    if (!file.is_open())
        throw FileOfElementsClosed();
    std::string line;
    while (std::getline(file, line)) {
        size_t d_pos = 0;
        int at_number;
        try {
            at_number = stoi(line.substr(0, d_pos = line.find(':')));
        } catch(const std::exception& e) {
            throw FileOfElementsReadException();
        }
        line = line.substr(d_pos+1);
        std::string symbol = line.substr(0, d_pos = line.find(':'));
        line = line.substr(d_pos+1);
        std::string name = line.substr(0, d_pos = line.find(':'));
        std::string color = line.substr(line.find('#')+1);
        r_elements.push_back(Element(at_number, symbol, name, color));
    }
    file.close();
    return r_elements;
}
