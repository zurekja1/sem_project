#include "EField.hpp"
#include <cstdlib>
#include <ctime>
#include <algorithm>

#define ELEMENT_START_COUNT 5
#if LETEST
    #define START_ELEMENT_RANGE std::pair<int, int>{114, 117}
#else
    #define START_ELEMENT_RANGE std::pair<int, int>{0, 4}
#endif
#define ELEMENT_FILE "element_tab.txt"
#if MECTEST
    #define MAX_ELEMENT_COUNT 10
#else
    #define MAX_ELEMENT_COUNT 30
#endif

// class controls game
class GameState {
private:
    const std::vector<Element> element_list;       // elements read from file to play this game

    std::pair<unsigned int, unsigned int> e_range; // range of generated elements
    Element held_element;                          // element for insertion
    EField field;                                  // field of elements
    
    // random int in element range
    inline int randIER() const;

public:
    // constructor
    GameState(std::string element_list_file);

    // destructor
    ~GameState();

    // create new held element form element range
    void createElement();

    // get actual held element
    Element getHeldElement() const;

    // insert held element to selected position
    void insertElement(std::size_t pos = 0);

    // removes element from field at selected position and set held element to electron
    void changeElement(std::size_t pos);

    // set held element as same element from field at selected position
    void duplicateElement(std::size_t pos);

    // erase and delete element from field at selected position
    void destroyElement(std::size_t pos);

    // erase element from field from selected position and set held element
    void moveElement(std::size_t pos);

    // return true, if field is mergeable
    bool isMergeable() const;

    // merge fields elements
    void merge();

    // set new range for element generating
    void setRange();

    // return field of elements copy
    std::vector<Element*> getField();

    // get amid position of element from angle
    std::size_t getAmidPosition(float angle);
    
    // get accurate position of element form angle
    std::size_t getAccurPosition(float angle);

    // clear field, set elemnt range and inser few elements for new game
    void reset();

    // return true for state of fullfilled filed of elements
    bool inProgress() const;

    //return true if field includes last element form element list
    bool hasLastElement();

    // return last element from element list and set held element on last element from element list
    Element getsetLastElement();

    #if TEST
        // output of object
        friend std::ostream& operator<<(std::ostream& out, const GameState& gs);
    #endif
};
