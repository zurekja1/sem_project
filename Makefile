CXX=g++
CXXLFLAGS= -c -Wall -pedantic -lSDL2 -o
TESTFLAGS= -DTEST=false -DMECTEST=false -DLETEST=false

all: color.o element.o efield.o gstate.o window.o layout.o render.o drawer.o main.o
	$(CXX) $^ -lSDL2 -o main
	rm *.o
	./main

main.o: main.cpp
	$(CXX) main.cpp $(CXXLFLAGS) main.o $(TESTFLAGS)

drawer.o: Drawer.cpp
	$(CXX) Drawer.cpp $(CXXLFLAGS) drawer.o $(TESTFLAGS)

render.o: Render.cpp
	$(CXX) Render.cpp $(CXXLFLAGS) render.o $(TESTFLAGS)

layout.o: Layout.cpp
	$(CXX) Layout.cpp $(CXXLFLAGS) layout.o $(TESTFLAGS)

window.o: Window.cpp
	$(CXX) Window.cpp $(CXXLFLAGS) window.o $(TESTFLAGS)

gstate.o: GameState.cpp
	$(CXX) GameState.cpp $(CXXLFLAGS) gstate.o $(TESTFLAGS)

efield.o: EField.cpp
	$(CXX) EField.cpp $(CXXLFLAGS) efield.o $(TESTFLAGS)

element.o: Element.cpp
	$(CXX) Element.cpp $(CXXLFLAGS) element.o $(TESTFLAGS)

color.o: Color.cpp
	$(CXX) Color.cpp $(CXXLFLAGS) color.o $(TESTFLAGS)