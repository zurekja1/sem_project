#include "Drawer.hpp"

// exception of wrong bmp file loading
const char* LoadBMPException::what() const throw() {
    return "BMP File Load Exception";
}

//constructor, ren subs of SDL_Renderer
//creats needed textures of atom, signs, numbers, uppers, lowers and background
Drawer::Drawer(Render* ren) : render(ren) {
    atom_image = SDL_LoadBMP(ATOM_TEMPLATE);
    atom_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), atom_image);
    if (atom_image == nullptr)
        throw LoadBMPException();
    
    signs_image = SDL_LoadBMP(SIGN_TEMPLATE);
    signs_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), signs_image);
    SDL_SetTextureColorMod(signs_texture, 0, 0, 0);
    if (signs_image == nullptr)
        throw LoadBMPException();

    numbers_image = SDL_LoadBMP(NUMBER_TEMPLATE);
    numbers_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), numbers_image);
    SDL_SetTextureColorMod(numbers_texture, 0, 0, 0);
    if (numbers_image == nullptr)
        throw LoadBMPException();

    uppers_image = SDL_LoadBMP(UPPERS_TEMPLATE);
    uppers_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), uppers_image);
    SDL_SetTextureColorMod(uppers_texture, 0, 0, 0);
    if (uppers_image == nullptr)
        throw LoadBMPException();

    lowers_image = SDL_LoadBMP(LOWERS_TEMPLATE);
    lowers_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), lowers_image);
    SDL_SetTextureColorMod(lowers_texture, 0, 0, 0);
    if (lowers_image == nullptr)
        throw LoadBMPException();

    background_image = SDL_LoadBMP(BG_TEMPLATE);
    background_texture = SDL_CreateTextureFromSurface(ren->getSDL_Render(), background_image);
    SDL_SetTextureColorMod(background_texture, 66, 42, 120);
    if (background_image == nullptr)
        throw LoadBMPException();
}

//destructor
//frees textures of atom, signs, numbers, uppers, lowers and background
Drawer::~Drawer() {
    SDL_DestroyTexture(atom_texture);
    SDL_FreeSurface(atom_image);

    SDL_DestroyTexture(signs_texture);
    SDL_FreeSurface(signs_image);
    
    SDL_DestroyTexture(numbers_texture);
    SDL_FreeSurface(numbers_image);
    
    SDL_DestroyTexture(uppers_texture);
    SDL_FreeSurface(uppers_image);

    SDL_DestroyTexture(lowers_texture);
    SDL_FreeSurface(lowers_image);
    
    SDL_DestroyTexture(background_texture);
    SDL_FreeSurface(background_image);
}

// get centerer position of window for element
SDL_Rect Drawer::getCenterPosition(const SDL_Rect* border) {
    SDL_Rect center_pos {
        border->x + (border->w - (int) ELEMENT_SIZE) / 2,
        border->y + (border->h - (int) ELEMENT_SIZE) / 2,
        (int) ELEMENT_SIZE, (int) ELEMENT_SIZE};
    return center_pos;
}

// draw text on pos area by elements atomic number and symbol
void Drawer::drawText(unsigned int an, const std::string symbol, const SDL_Rect* pos) {
    if (an == 0) { // element is proton, electron, duplicator, mover | black_hole
        SDL_Rect s_pos = { pos->x + pos->w / 4, pos->y + pos->h / 4, pos->w / 2, pos->h / 2};
        SDL_Rect selection = {SIGN_WIDTH / SIGN_LEN, 0, SIGN_WIDTH / SIGN_LEN, SIGN_HEIGHT};
        if      (symbol == "+") selection.x *= 0;
        else if (symbol == "-") selection.x *= 1;
        else if (symbol == "=") selection.x *= 2;
        else if (symbol == ">") selection.x *= 3;
        else                    selection.x *= 4;
        SDL_RenderCopy(render->getSDL_Render(), signs_texture, &selection, &s_pos);
    } else { // element with regular atomic number
        std::string str_number = std::to_string(an);
        int f_pos = (str_number.size() == 1 ? 5 : (str_number.size() == 2 ? 4 : 3));
        for (size_t i = 0; i < str_number.size(); i++) {
            SDL_Rect s_pos = {NUMBER_WIDTH / NUMBER_LEN * (str_number[i] - '0'), 0, NUMBER_WIDTH / NUMBER_LEN, NUMBER_HEIGHT};
            SDL_Rect n_pos = {pos->x + pos->w * (f_pos + (int) i * 2) / 12,
                     pos->y + pos->h * 1 / 8,
                     pos->w / 6,
                     pos->h / 4};
            SDL_RenderCopy(render->getSDL_Render(), numbers_texture, &s_pos, &n_pos);
        }
        f_pos = symbol.size() == 1 ? 5 : 4;
        for (size_t i = 0; i < symbol.size(); i++) {
            SDL_Rect s_pos = {CASE_WIDTH / CASE_LEN * (symbol[i] - (i == 0 ? 'A' : 'a')), 0, CASE_WIDTH / CASE_LEN, CASE_HEIGHT};
            SDL_Rect n_pos = {pos->x + pos->w * (f_pos + (int) i * 2) / 12,
                     pos->y + pos->h * 4 / 8,
                     pos->w / 6,
                     pos->h / 4};
            SDL_RenderCopy(render->getSDL_Render(), (i == 0 ? uppers_texture : lowers_texture), &s_pos, &n_pos);
        }
    }
}

// draw element to inserted positon on window
void Drawer::drawElement(const Element* e, SDL_Rect* pos) {
    SDL_Color c = e->getColor().toSDL_Color();
    render->setColor(c);
    #if TEST
        SDL_RenderDrawRect(render->getSDL_Render(), pos);
    #endif
    SDL_SetTextureColorMod(atom_texture, c.r, c.g, c.b);
    SDL_RenderCopy(render->getSDL_Render(), atom_texture, NULL, pos);
    drawText(e->getAN(), e->getSymbol(), pos);
}

//draw image of bacground into window
void Drawer::drawBackground() {
    SDL_RenderCopy(render->getSDL_Render(), background_texture, NULL, render->getW_Layout().getBorder());
}

//draw held Element into center of window
void Drawer::drawHeldElement(const Element& h_e) {
    SDL_Rect he_pos = getCenterPosition(render->getW_Layout().getBorder());
    drawElement(&h_e, &he_pos);
}

//draw Element form field around of center of window
void Drawer::drawElementField(const std::vector<Element*>& field) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    float phi = M_PI * 2 / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (size_t i = 0; i < field.size(); i++) {
        SDL_Rect e_pos = {
            center_pos.x - (int) (sinf((float) (i * phi)) * radius),
            center_pos.y - (int) (cosf((float) (i * phi)) * radius),
            center_pos.w, center_pos.h};
        drawElement(field[i], &e_pos);
    }
}

// draw effect of showing held element with field around
void Drawer::drawElementShowEffect(const std::vector<Element*>& field, const Element& h_elem) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    drawBackground();       // drawing background
    drawElementField(field);// draweing field around center
    for (float f = 0.0f; f < 1.0f; f += 2.0f / FPS) { // redrawing of held elemt into center
        SDL_Rect h_pos = {
            center_pos.x + (int) ELEMENT_SIZE / 2 - (int) (center_pos.w * f) / 2 ,
            center_pos.y + (int) ELEMENT_SIZE / 2 - (int) (center_pos.h * f) / 2 ,
            (int) (center_pos.w * f), (int) (center_pos.h * f)};
        drawElement(&h_elem, &h_pos);
        render->show();
        SDL_Delay(ANIM_DELAY);
    }
    render->clear();
}

// draw effect of insrting elemnt form center to position of field
void Drawer::drawInsertEffect(const std::vector<Element*>& field, const Element& h_elem,
                              const std::size_t& pos) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);

    float phi = M_PI * 2 * (pos - 0.5) / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (float f = 0.0f; f <= 1.0f; f += 1.5f/FPS) { // drawing elemt inserting into field to pos
        drawBackground();
        drawElementField(field);
        SDL_Rect h_pos = {
            center_pos.x - (int) (sinf((float) (phi)) * radius * f),
            center_pos.y - (int) (cosf((float) (phi)) * radius * f),
            center_pos.w, center_pos.h};
        drawElement(&h_elem, &h_pos);
        render->show();
        SDL_Delay(ANIM_DELAY);
        render->clear();
    }
    
    //calculation of trajectories from old to new positions by angel
    std::vector<std::pair<float,float>> ftangel(field.size()); // from to
    for (size_t i = 0; i < field.size(); i++)
        ftangel[i].first = M_PI * 2 * i / field.size();
    ftangel.insert(ftangel.begin() + pos, std::pair<float, float> {M_PI * 2 * (pos - 0.5) / field.size(), 0});
    for (size_t i = 0; i < ftangel.size(); i++)
        ftangel[i].second = M_PI * 2  * i / (field.size()+1);
    
    // drawing element moves from old to new position
    for (float f = 0.0f; f <= 1.0f; f += 1.5f/FPS) {
        drawBackground();
        float from, to;
        for (size_t i = 0; i < pos; i++) {
            from = ftangel[i].first;
            to = ftangel[i].second;
            SDL_Rect e_pos = {
                center_pos.x - (int) (sinf((float) (from + (to - from) * f)) * radius),
                center_pos.y - (int) (cosf((float) (from + (to - from) * f)) * radius),
                center_pos.w, center_pos.h};
            drawElement(field[i], &e_pos);
        }

        from = ftangel[pos].first;
        to = ftangel[pos].second;
        SDL_Rect e_pos = {
            center_pos.x - (int) (sinf((float) (from + (to - from) * f)) * radius),
            center_pos.y - (int) (cosf((float) (from + (to - from) * f)) * radius),
            center_pos.w, center_pos.h};
            drawElement(&h_elem, &e_pos);

        for (size_t i = pos; i < field.size(); i++) {
            from = ftangel[i+1].first;
            to = ftangel[i+1].second;
            SDL_Rect e_pos = {
                center_pos.x - (int) (sinf((float) (from + (to - from) * f)) * radius),
                center_pos.y - (int) (cosf((float) (from + (to - from) * f)) * radius),
                center_pos.w, center_pos.h};
            drawElement(field[i], &e_pos);
        }
        render->show();
        SDL_Delay(ANIM_DELAY/2);
        render->clear();
    }

}

// draw effect of removing element from position to center of window
void Drawer::drawRemoveEffect(const std::vector<Element*>& field, const std::size_t& pos) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);

    float phi = M_PI * 2 / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (float f = 0.0f; f <= 1.0f; f += 1.5f/FPS) { // drawing element removing from field to center of window
        drawBackground();
        for (size_t i = 0; i < field.size(); i++) {
            if (i == pos) continue;
                SDL_Rect e_pos = {
                    center_pos.x - (int) (sinf((float) (phi * i)) * radius),
                    center_pos.y - (int) (cosf((float) (phi * i)) * radius),
                    center_pos.w, center_pos.h};
                drawElement(field[i], &e_pos);
        }
        SDL_Rect h_pos = {
            center_pos.x - (int) (sinf((float) (phi * pos)) * radius * (1 - f)),
            center_pos.y - (int) (cosf((float) (phi * pos)) * radius * (1 - f)),
            center_pos.w, center_pos.h};
        drawElement(field[pos], &h_pos);

        render->show();
        SDL_Delay(ANIM_DELAY/2);
        render->clear();
    }

    //calculation of trajectories from old to new positions by angel
    std::vector<std::pair<float,float>> ftangel(field.size());
    for (size_t i = 0; i < field.size(); i++)
        ftangel[i].first = M_PI * 2 * i / field.size();
    ftangel.erase(ftangel.begin() + pos);
    for (size_t i = 0; i < ftangel.size(); i++)
        ftangel[i].second = M_PI * 2 * i / ftangel.size();
    
    // drawing element moves from old to new position
    for (float f = 0.0f; f <= 1.0f; f += 1.5f/FPS) {
        drawBackground();
        float from, to;
        for (size_t i = 0; i < ftangel.size(); i++) {
            from = ftangel[i].first;
            to = ftangel[i].second;
            SDL_Rect e_pos = {
                center_pos.x - (int) (sinf((float) (from + (to - from) * f)) * radius),
                center_pos.y - (int) (cosf((float) (from + (to - from) * f)) * radius),
                center_pos.w, center_pos.h};
            drawElement(field[i >= pos ? i + 1 : i], &e_pos);
        }

        SDL_Rect h_pos = {
            center_pos.x, center_pos.y,
            center_pos.w, center_pos.h};
        drawElement(field[pos], &h_pos);

        render->show();
        SDL_Delay(ANIM_DELAY/2);
        render->clear();
    }
}

// drawing elements merging into proton element
void Drawer::drawMergeEffect(const std::vector<Element*>& field) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;

    // finding positions of mergeable elements and proton
    std::size_t proton_pos, r_pos, l_pos;
    for (size_t i = 0; i < field.size(); i++) {
        r_pos = (field.size() + i + 1) % field.size();
        l_pos = (field.size() + i - 1) % field.size();
        if (*field[l_pos] == *field[r_pos] && *field[i] == proton){
            proton_pos = i;
            break;
        }
    }
    
    //calculation and setting of trajectories from old to new positions by angel
    std::vector<std::pair<float, float>> ftangel(field.size());
    for (size_t i = 0; i < field.size(); i++)
        ftangel[i].first = M_PI * 2 * i / field.size();
    std::size_t counter = 0;
    for (size_t i = 0; i < field.size(); i++)
        if (i != r_pos && i != l_pos)
            ftangel[i].second = M_PI * 2 * counter++ / (field.size() - 2);

    ftangel[r_pos].second = ftangel[proton_pos].second;
    ftangel[l_pos].second = ftangel[proton_pos].second;

    if (proton_pos == field.size() - 1) ftangel[r_pos].first  = M_PI * 2;
    else if (proton_pos == 0)           ftangel[l_pos].second = M_PI * 2;

    // drawing merging elements into proton
    for (float f = 0.0f; f <= 1.0f; f += 1.5f/FPS) {
        drawBackground();
        float from, to;
        for (size_t i = 0; i < ftangel.size(); i++) {
            from = ftangel[i].first;
            to = ftangel[i].second;
            SDL_Rect e_pos = {
                center_pos.x - (int) (sinf((float) (from + (to - from) * f)) * radius),
                center_pos.y - (int) (cosf((float) (from + (to - from) * f)) * radius),
                center_pos.w, center_pos.h};
            drawElement(field[i], &e_pos);
        }
        render->show();
        SDL_Delay(ANIM_DELAY/2);
        render->clear();
    }
}

// drawing effect before first insert
void Drawer::drawStartEffect(const std::vector<Element*>& field, const Element& h_elem) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    float phi = M_PI * 2 / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (float f = 0.0f; f <= 1.0f; f += 1.0f/FPS) { // drawing elemets from center to radius
        drawBackground();
        for (size_t i = 0; i < field.size(); i++) {
            SDL_Rect h_pos = {
            center_pos.x - (int) (sinf((float) (i * phi)) * radius * f),
            center_pos.y - (int) (cosf((float) (i * phi)) * radius * f),
            center_pos.w, center_pos.h};
            drawElement(field[i], &h_pos);
        }
        drawElement(&h_elem, &center_pos);
        render->show();
        SDL_Delay(ANIM_DELAY);
        render->clear();
    }
}

// drawing effect after reach last element
void Drawer::drawEndingEffect(const std::vector<Element*>& field, const Element& h_elem) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    float phi = M_PI * 2 / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (float f = 0.0f; f <= M_PI * 4; f += 0.75f/FPS) { // gradual drawing field elements to center
        drawBackground();
        for (size_t i = 0; i < field.size(); i++) {
            SDL_Rect h_pos = {
            center_pos.x - (int) (sinf((float) (f * f + i * phi)) * radius * (1 - f / (M_PI * 4))),
            center_pos.y - (int) (cosf((float) (f * f + i * phi)) * radius * (1 - f / (M_PI * 4))),
            center_pos.w, center_pos.h};
            drawElement(field[i], &h_pos);
        }
        render->show();
        SDL_Delay(ANIM_DELAY);
        render->clear();
    }
    drawElementShowEffect({}, h_elem);
}

// drawing effect of reseting elements
void Drawer::drawResetEffect(const std::vector<Element*>& field, const Element& h_elem) {
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    float phi = M_PI * 2 / field.size();
    int radius = (border->w - (int) ELEMENT_SIZE ) / 2;
    for (float f = 0.0f; f <= 1.0f; f += 0.5f/FPS) { // gradual drawing field elements to center
        drawBackground();
        for (size_t i = 0; i < field.size(); i++) {
            SDL_Rect h_pos = {
            center_pos.x - (int) (sinf((float) (i * phi)) * radius * (1 - f * f)),
            center_pos.y - (int) (cosf((float) (i * phi)) * radius * (1 - f * f)),
            center_pos.w, center_pos.h};
            drawElement(field[i], &h_pos);
        }
        drawElement(&h_elem, &center_pos);
        render->show();
        SDL_Delay(ANIM_DELAY);
        render->clear();
    }
}

// drawing signs for last 3 elements
void Drawer::drawCountdownEffect(const std::vector<Element*>& field) {
    if (field.size() < MAX_ELEMENT_COUNT - 3) return;
    
    SDL_Rect* border = render->getW_Layout().getBorder();
    SDL_Rect center_pos = getCenterPosition(border);
    int radius = (border->w - (int) ELEMENT_SIZE ) / 4;
    SDL_Rect h_pos;
    if (MAX_ELEMENT_COUNT - field.size() == 3) { // drawing signs for last 3 elements
        for (int i = 0; i < 3; i++) {
            h_pos = {
                center_pos.x + center_pos.w / 2 - center_pos.w * 3 / 8 + center_pos.w / 4 * i,
                center_pos.y - radius,
                center_pos.w / 4, center_pos.h / 4};
            SDL_SetTextureColorMod(atom_texture, 128, 128, 128);
            SDL_RenderCopy(render->getSDL_Render(), atom_texture, NULL, &h_pos);
        }
    } else if (MAX_ELEMENT_COUNT - field.size() == 2) { // drawing signs for last 2 elements
        for (int i = 0; i < 2; i++) {
            h_pos = {
                center_pos.x + center_pos.w / 2 - center_pos.w / 4 + center_pos.w / 4 * i,
                center_pos.y - radius,
                center_pos.w / 4, center_pos.h / 4};
            SDL_SetTextureColorMod(atom_texture, 128, 128, 128);
            SDL_RenderCopy(render->getSDL_Render(), atom_texture, NULL, &h_pos);
        }
    } else { // drawing signs for last 1 element
        h_pos = {
            center_pos.x + center_pos.w / 2 - center_pos.w / 8,
            center_pos.y - radius,
            center_pos.w / 4, center_pos.h / 4};
        SDL_SetTextureColorMod(atom_texture, 128, 128, 128);
        SDL_RenderCopy(render->getSDL_Render(), atom_texture, NULL, &h_pos);
    }
}
