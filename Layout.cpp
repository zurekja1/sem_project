#include "Layout.hpp"

// constructor
Layout::Layout() {
    border = SDL_Rect{0, 0, 0, 0};
}

// constructor, make border of game panel
Layout::Layout(int width, int height) {
    resize(width, height);
}

// destructor
Layout::~Layout() {}

// reshape border of game panel
void Layout::resize(int width, int height) {
    int n_size = width < height ? width : height;
    border.x = width  / 2 - n_size / 2 + HOR_BORDER;
    border.y = height / 2 - n_size / 2 + VER_BORDER;
    border.w = n_size - 2 * HOR_BORDER;
    border.h = n_size - 2 * VER_BORDER;
}

// return poiter of game panel
SDL_Rect* Layout::getBorder() {
    return &border;
}

// return true, if inserted coordinates are inside of game panel
bool Layout::isClicked(int x, int y) {
    return (border.x < x) && (x < (border.x + border.w)) &&
           (border.y < y) && (y < (border.y + border.h));
}

// return angel in radians from center to inserted coordinates
float Layout::getAngleFromCenter(int x, int y) {
    int xc = border.w / 2 + border.x;
    int yc = border.h / 2 + border.y;
    std::pair<float, float> u = {x - xc, y - yc};
    std::pair<float, float> v = {0, - yc};
    float un = sqrtf(u.first * u.first + u.second * u.second);
    float vn = sqrtf(v.first * v.first + v.second * v.second);
    float angle = acosf((u.first * v.first + u.second * v.second) / (un * vn));
    return (x < xc ? angle : 2 * M_PI - angle);
}