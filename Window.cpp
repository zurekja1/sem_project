#include "Window.hpp"

// exception, window is not created
const char* WindowException::what() const throw() {
    return "Window creation failed";
}

// constructor
Window::Window(const char *title, int width, int height)
    : win(SDL_CreateWindow(
        title, WINDOW_POS, WINDOW_POS,
        width,  height, WIN_FLAGS)),
        win_process(Process::OPENED) {
            if (win == nullptr) throw WindowException();
            SDL_SetWindowResizable(win, SDL_TRUE);
        }

//destructor
Window::~Window() {
    SDL_DestroyWindow(win);
}

// return pointer of sdl_window
SDL_Window* Window::getSDL_Window() const {
    return win;
}

// return true, if window process is equal to opened
bool Window::isOpened() const {
    return win_process == Process::OPENED;
}

// set window process to closed
void Window::stop() {
    win_process = Process::CLOSED;
}
