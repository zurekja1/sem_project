#include "SDL2/SDL.h"
#include "Drawer.hpp"

// merging process part
void mergePart(Drawer& dr, GameState& gs) {
    while (gs.isMergeable() && !gs.hasLastElement()) {
        dr.drawMergeEffect(gs.getField());
        gs.merge();
        dr.drawElementField(gs.getField());
        SDL_Delay(MERGE_DELAY);
        gs.setRange();
    }
}

// if held element is black hole, this part is in process
void blackHolePart(Drawer& dr, GameState& gs) {
    for (size_t i = 0; i < gs.getField().size()/2; i++) {
        std::vector<Element*> tmp_elements = gs.getField();
        std::size_t r_pos = rand() % tmp_elements.size();
        dr.drawRemoveEffect(gs.getField(), r_pos);
        gs.destroyElement(r_pos);
        SDL_Delay(DELAY);
    }
    gs.createElement();
    mergePart(dr, gs);
    dr.drawElementShowEffect(gs.getField(), gs.getHeldElement());
}

// if held element is duplicator, this part is in process
void duplicatorPart(Drawer& dr, GameState& gs, const float angle) {
    std::size_t pos = gs.getAccurPosition(angle);
    gs.duplicateElement(pos);
    mergePart(dr, gs);
    dr.drawElementShowEffect(gs.getField(), gs.getHeldElement());
}

// if held element is electron, this part is in process
void electronPart(Drawer& dr, GameState& gs, const float angle) {
    std::size_t pos = gs.getAccurPosition(angle);
    dr.drawRemoveEffect(gs.getField(), pos);
    gs.changeElement(pos);
    mergePart(dr, gs);
    dr.drawElementShowEffect(gs.getField(), gs.getHeldElement());
}

// if held element is mover, this part is in process
void moverPart(Drawer& dr, GameState& gs, const float angle) {
    std::size_t pos = gs.getAccurPosition(angle);
    dr.drawRemoveEffect(gs.getField(), pos);
    gs.moveElement(pos);
    mergePart(dr, gs);
}

// if held element is proton or element from element list, this part is in process
void insertElementPart(Drawer& dr, GameState& gs, const float angle) {
    std::size_t pos = gs.getAmidPosition(angle);
    dr.drawInsertEffect(gs.getField(), gs.getHeldElement(), pos);
    gs.insertElement(pos);
    gs.createElement();
    mergePart(dr, gs);
    dr.drawElementShowEffect(gs.getField(), gs.getHeldElement());
}

int main(void) {
    SDL_Init(SDL_INIT_EVERYTHING);

    Window window = Window(WIN_TITLE, WIN_WIDTH, WIN_HEIGHT);
    Render render = Render(window);
    Drawer drawer = Drawer(&render);
    GameState gameS(ELEMENT_FILE);

    drawer.drawStartEffect(gameS.getField(), gameS.getHeldElement());
    #if TEST
        std::cout << gameS << std::endl;
    #endif

    while(window.isOpened()) { // true until the window ends
        SDL_Event e;
        // the game is in progess or element field has not the last element from element list
        if (gameS.inProgress() && !gameS.hasLastElement()) {
            while (SDL_PollEvent(&e)) {
                switch (e.type) {
                    case SDL_QUIT:              // break the while cycle
                        window.stop();
                        break;
                    case SDL_WINDOWEVENT:       // resize render layout
                        if (e.window.event == SDL_WINDOWEVENT_RESIZED)
                            render.resize(window.getSDL_Window());
                        break;
                    case SDL_MOUSEBUTTONDOWN:   // only responds to clicks in the boarder
                        int x, y;
                        SDL_GetMouseState(&x, &y);
                        if (render.getW_Layout().isClicked(x, y)) {
                            float angle = render.getW_Layout().getAngleFromCenter(x, y);
                            Element held_e = gameS.getHeldElement();
                            if      (held_e == black_hole) blackHolePart(drawer, gameS);
                            else if (held_e == duplicator) duplicatorPart(drawer, gameS, angle);
                            else if (held_e == electron)   electronPart(drawer, gameS, angle);
                            else if (held_e == mover)      moverPart(drawer, gameS, angle);
                            else                           insertElementPart(drawer, gameS, angle);
                            #if TEST
                                std::cout << gameS << std::endl;
                            #endif
                        }
                        break;
                    default:
                        break;
                }
            }
            // drawing special events
            if (gameS.hasLastElement())
                drawer.drawEndingEffect(gameS.getField(), gameS.getsetLastElement());
            if (!gameS.inProgress())
                drawer.drawResetEffect(gameS.getField(), gameS.getHeldElement());
        } else { // the game is not in progess or element field has the last element from element list
            while (SDL_PollEvent(&e)) {
                switch (e.type) {
                    case SDL_QUIT:              // break the while cycle
                        window.stop();
                        break;
                    case SDL_WINDOWEVENT:       // resize render layout
                        if (e.window.event == SDL_WINDOWEVENT_RESIZED)
                            render.resize(window.getSDL_Window());
                        break;
                    case SDL_MOUSEBUTTONDOWN:   // start new game on click in boarder
                        int x, y;
                        SDL_GetMouseState(&x, &y);
                        if (render.getW_Layout().isClicked(x, y)) {
                            gameS.reset();
                            drawer.drawStartEffect(gameS.getField(), gameS.getHeldElement());
                            #if TEST
                                std::cout << gameS << std::endl;
                            #endif
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        // drawing routine
        drawer.drawBackground();
        if (gameS.inProgress() && !gameS.hasLastElement()) {
            drawer.drawElementField(gameS.getField());
            drawer.drawCountdownEffect(gameS.getField());
        }
        drawer.drawHeldElement(gameS.getHeldElement());

        render.show();
        render.clear();
        SDL_Delay(DELAY);
    }

    SDL_Quit();
    return 0;
}