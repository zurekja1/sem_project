#include <iostream>
#include "Color.hpp"

class Element {
    private:
        unsigned int atomic_number;
        std::string symbol;
        std::string name;
        Color color;

    public:
        Element();

        Element(const unsigned int atomic_number,
                const std::string& symbol,
                const std::string& name,
                const std::string& color_s);

        //destructor
        ~Element();

        // copy constructor
        Element(const Element& e);

        // overoaded operator =
        Element& operator=(const Element& e);

        // gets elements atomic number
        unsigned int getAN() const;

        // sets elements atomic number
        void setAN(unsigned int n_AN);

        // gets elements color
        Color getColor() const;

        // gets elements symbol
        std::string getSymbol() const;

        // overloaded operator ==
        friend bool operator==(const Element& lhs, const Element& rhs);

        // overloaded operator !=
        friend bool operator!=(const Element& lhs, const Element& rhs);

        #if TEST
            // output of object
            friend std::ostream& operator<<(std::ostream& out, const Element& e);
        #endif
};
// static cosnt elements for game rules
static const Element proton     = Element(0, "+", "proton",     "e13929ff");
static const Element electron   = Element(0, "-", "electron",   "686cfaff");
static const Element duplicator = Element(0, "=", "duplicator", "68faa1ff");
static const Element mover      = Element(0, ">", "mover",      "e6fa68ff");
static const Element black_hole = Element(0, "#", "black_hole", "423e5eff");