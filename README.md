## Semestrální projekt Atoms
Semestrální projekt **Atoms** je hra inspirovaná puzzle hrou na mobilní telefony **Atomas**.
Cílem hry je spojovat elementy z periodické tabulky, tedy od *H<sub>1</sub>*, a dosáhnout posledního elementárního prvku *Og<sub>118</sub>*. Hráč se během hry setká se superelementy, které umožňují upravovat hráčovo pole podle pravidel.

### Pravidla hry
Hráč nejprve dostane do svého herního pole *5* náhodných elementárních prvků v rozsahu H<sub>1</sub> až Be<sub>4</sub>. Uprostřed pole se nachází prvek, který hráč drží a který hráč musí vložit kamkoliv do pole.
Aby hráč dosáhl posledního elementárního prvku *Og<sub>118</sub>*, jsou přidány superprvky. Ty umožňují s prvky různě zacházet podle tabulky:
| symbol | použití |
|--|--|
| **+** | <li> sloučí dva stejné elementární prvky do nového prvku s vyšším atomovým číslem <li> pokud to bude možné, hráč může spustit řetězovou reakci a sloučí tak více prvků do jednoho nového <li> může upravit rozsah prvků, které se hráči náhodně generují|
| - | <li> hráč vezme element z herního pole <li>sebraný element se změní na **+**|
| > | <li> hráč vezme element z herního pole a dostane možnost element přemístit |
| = | <li> hráč vytvoří kopii elementu, který si vybere |
| ~ | <li> náhodně odstraní z hráčova pole necelou polovinu umístěných elementů <li> hráč nemůže nic ovlivnit|

Celá hra tak záleží na hráčově strategii, jak se od prvního elementu dostane k poslednímu. Hra skončí, jakmile hráč vyplní pole, maximální počet prvků v poli je *30*. Jakmile hráč bude mít poslední *3* volné pozice, bude na to upozorněn *1* až *3* puntíky nad drženým elementem.

### Spuštění
> V repozitáři se nachází soubory *.hpp* & *.cpp* , které byly určené pro vývoj samotné hry. Hra byla naprogramována na linuxové distribuci Ubuntu a pro grafické rozhraní byla použita multiplatformní vývojová knihovna [SDL2](https://www.libsdl.org/).

 #### Vývojová verze
 Pro spuštění vývojové verze je potřeba mít nainstalovanou **SDL2** knihovnu, např. pomocí příkazu`sudo apt-get install libsdl2-dev`. Pro zkompilování stačí zadat příkaz `make` a následně se hra spustí, nebo lze hru poté spustit příkazem `./main`.
 Pro testy jsou pro kompilaci předdefinována makra na hodnotu false. Přepsáním hodnoty na true se testy projeví viz tabulka:
| Název | význam |
|--|--|
| *TEST* | <li> vypisuje do konzole rozsah elementů, které hráč může dostat <li> vypisuje do konzole element, který hráč drží a musí ho použít <li> vypisuje do konzole celé pole elementů <li> vyznačení elementů <li> grafické znázornění pole, ve kterém se vyskytují elementy a hráč může elementy používat |
| *MECTEST * | <li> Maximum element count <li> nastaví maximální počet elementů z *30* na *10* <li> ošetření zaplnění pole elementů |
| *LETEST* | <li> nastaví počáteční rozsah elementů na rozsah elementů *< Fl<sub>117</sub>, Ts<sub>117</sub> >* <li> ošetření dosáhnutí posledního elementu |
 
 #### Verze na Windows
V repozitáři se nachází složka *sem_project_win*, ve které je verze spustitelná na OS Windows. Obsahuje soubory, které jsou potřebné pro zkompilování *.hpp* & *.cpp* souborů. Pro zkompilování je potřeba spustit příkaz `make`.
