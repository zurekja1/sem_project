#include <vector>
#include "Element.hpp"
#include <fstream>

// exception of wrong field position
struct EFieldArrayException : std::exception {
    virtual const char* what() const throw();
};

// exception of wrong file of element to read
struct FileOfElementsClosed : std::exception {
    virtual const char* what() const throw();
};

// exception of wrong elemet to read
struct FileOfElementsReadException : std::exception {
    virtual const char* what() const throw();
};

// class for holding elements in order
class EField {
    private:
        std::vector<Element*> field;

    public:

        // constructor
        EField();

        // destructor, removes and delete all inserted elements
        ~EField();

        // insert element to selected position
        void insert(const Element& nElem, std::size_t pos = 0);

        // insert vector of elements to selected position
        void insert(std::vector<Element> nElems, std::size_t pos = 0);

        // remove element from selected position
        Element* remove(std::size_t pos = 0);

        // remove elements from selected position from -> to
        std::vector<Element*> remove(std::size_t from, std::size_t to);

        // overloaded operator []
        Element* operator[](std::size_t idx);

        // overloaded operator []
        const Element* operator[](std::size_t idx) const;

        // get actual size of element field
        std::size_t getSize() const;

        // return true if at pos is proton and around same elements
        bool isBridge(std::size_t pos, size_t width) const;

        #if TEST
            // output of object
            friend std::ostream& operator<<(std::ostream& out, const EField& ef);
        #endif
};

std::vector<Element> readElementFile(const std::string& f_name);