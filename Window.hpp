#include "SDL2/SDL_video.h"
#include <exception>

#define WIN_TITLE "ATOMS"
#define WINDOW_POS SDL_WINDOWPOS_CENTERED
#define WIN_WIDTH   800
#define WIN_HEIGHT  600
#define WIN_FLAGS   0

// exception, window is not created
struct WindowException : std::exception {
    virtual const char* what() const throw();
};

enum Process { OPENED, CLOSED };

// class representingc sdl_window
class Window {
    private:
        SDL_Window* win;

        Process win_process;

    public:
        // constructor
        Window(const char *title, int width, int height);   

        //destructor
        ~Window();

        // return pointer of sdl_window
        SDL_Window* getSDL_Window() const;

        // return true, if window process is equal to opened
        bool isOpened() const;

        // set window process to closed
        void stop();
};
